$(document).ready(function() {
  // Extracting the authentication token.
  var tokenRegex = new RegExp("access_token=(.[^&]+)&token");
  var stringToExtract = $(location).attr("href");
  var token = stringToExtract.match(tokenRegex);
  
  if (token == null) {
    console.log("ERROR"); //TODO: Give error.
  } else {
    token = token[1];
    // Changing the current URL.
    history.replaceState(null, null, "/visualize");

    handleInformation(token);
  }
});

function handleInformation(token) {
  var genres = [];
  var artists = [];
  var songs = [];

  // Calls the getArtistsAndGenres methods then puts them in the respective folder.
  getArtistsAndGenres(token, 0)
    .then(function(data) {
      data.items.forEach(function(artist, index) {
        artists.push(artist.name.toLowerCase());

        for (let genre of artist.genres) {
          genres.push(genre.toLowerCase());
        }
      });
    })
    .then(function() {
      makeBars(genres);
      getRecommendations(token, genres, 0)
        .then(function(data) {
          recommendArtists(artists, data);
        });
    });

  getTracks(token, 0)
    .then(function(data) {
      data.items.forEach(function(song, index) {
        songs.push(song.name.toLowerCase());
      });
    })
    .then(function() {
      ; // Do something.
    });
}

function getArtistsAndGenres(token, offset) {
  return fetch(
    `https://api.spotify.com/v1/me/top/artists?offset=${offset}&limit=50`,
    {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token
      },
      json: true
    }
  ).then(response => response.json());
}

function getTracks(token, offset) {
  return fetch(
    `https://api.spotify.com/v1/me/top/tracks?offset=${offset}&limit=50`,
    {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token
      },
      json: true
    }
  ).then(response => response.json());
}

function getRecommendations(token, genres, offset) {
  genres = encodeURI(genres.slice(0, 5).join(","));
  return fetch(
    `https://api.spotify.com/v1/recommendations?market=US&seed_genres=${genres}&min_energy=0.4&min_popularity=50`,
    {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token
      },
      json: true
    }
  ).then(response => response.json());
}

function makeBars(genres) {
  var genreCount = {};
  var labelsArray = [];
  var dataArray = [];

  for (let genre of genres) {
    if (genre in genreCount) {
      genreCount[genre] += 1;
    } else {
      genreCount[genre] = 1;
    }
  }

  // Sorting the values.
  var values = Object.values(genreCount);
  values.sort(function(a,b) {return b-a;});
  
  for (let i = 0; i < 10; i++) {
    // Finding the appropriate key.
    for (let key of Object.keys(genreCount)) {
      // If found then push the key and data to the appropriate array.
      if (genreCount[key] === values[i]) {
        // Capitalising every letter of the word.
        let name = key.split(' ')
                      .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
                      .join(' ');

        labelsArray.push(name);
        dataArray.push(values[i]);
        delete genreCount[key];
        break;
      }
    }
  }

  // Adds the data to the radar.
  var ctx = document.getElementById("bars").getContext("2d");
  var chart = new Chart(ctx, {
    type: "bar",
    data: {
      labels: labelsArray,
      datasets: [{
        data: dataArray,
        backgroundColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)'
      ],
      borderColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)'
      ]
      }]
    },
    options: {
      title: {
        display: true,
        text: "Songs Listened To Per Genre"
      },
      legend: {
        display: false
      },
      responsive: false,
      maintainAspectRatio: true,
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  });
}

function recommendArtists(artists, data) {
  let tracks = data.tracks;
  let recommendedArtists = {};
  let count = 0;

  for (let track of tracks) {
    artistName = track.artists[0].name.toLowerCase();
    artistLink = track.artists[0].external_urls["spotify"];
    artistImage = track["album"]["images"][0]["url"];
    if (artistName in recommendedArtists) {
      ;
    } else {
      if (count < 12) {
        recommendedArtists[artistName] = {
          "link": artistLink,
          "image": artistImage
        }
        count += 1;
      } else {
        break;
      }
    }
  }

  let imageCount = 0;

  $("#imageText").text("Recommended Artists");

  for (let key of Object.keys(recommendedArtists)) {
    let artist = recommendedArtists[key];
    console.log(artist);
    if (imageCount == 4) {
      $(".imageContainer").append("<br>");
      imageCount = 0;
    }

    $(".imageContainer").append(`<a href="${artist["link"]}"><img src="${artist["image"]}" id="containerImage"></a>`);
    imageCount += 1;
  }
}