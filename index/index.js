$("#authenticateButton").on("click", function() {
  let authenticationURL =
    "https://accounts.spotify.com/authorize" +
    "?client_id=" +
    "585d5fba12b74818a570115f5545b3ad" +
    "&redirect_uri=" +
    encodeURIComponent("https://vassiliosv.gitlab.io/spotify-visualizer/visualize/") +
    "&scope=" +
    encodeURIComponent("user-top-read") +
    "&response_type=token";

  window.location = authenticationURL;
});
